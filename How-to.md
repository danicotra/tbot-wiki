We have several How-to guides for you to help you implementing different
features of your bot the right way.

- [How to send messages](./How-to/How-to-send-messages);
- [How to use webhooks](./How-to/How-to-use-webhooks).

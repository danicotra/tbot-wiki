In this wiki, you can find:

- A [tutorial] to get started with `tbot`;
- Several [How-to guides] to implement different features of your bot with
  `tbot` the right way;
- Our design principles of `tbot` and why it's the way it is.

API reference can be found on *[docs.rs]*.

[tutorial]: ./Tutorial
[How-to guides]: ./How-to
[docs.rs]: https://docs.rs/tbot

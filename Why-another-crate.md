There are two popular crates at this time: `telebot` and `telegram-bot`. If
either one was good enough, there would be no need in making another one.
Let's look at what's wrong with both of them and see how `tbot` is better at
it.

# Complexity

Let's look at the examples in their readme of each crate (slightly simplified
and formatted with `rustfmt`'s default config):

```rust
// telegram-bot
use futures::Stream;
use telegram_bot::*;
use tokio_core::reactor::Core;

let mut core = Core::new().unwrap();

let token = std::env::var("TELEGRAM_BOT_TOKEN").unwrap();
let api = Api::configure(token).build(core.handle()).unwrap();

core.run(api.stream().for_each(|update| {
    if let UpdateKind::Message(message) = update.kind {
        if let MessageKind::Text { ref data, .. } = message.kind {
            api.spawn(message.text_reply(format!(
                "Hi, {}! You just wrote '{}'",
                &message.from.first_name, data
            )));
        }
    }

    Ok(())
})).unwrap();
```

```rust
// telebot
use futures::{stream::Stream, Future};
use telebot::{bot, functions::*};
use tokio_core::reactor::Core;

let mut lp = Core::new().unwrap();
let bot = bot::RcBot::new(lp.handle(), "<TELEGRAM-BOT-TOKEN>").update_interval(200);

bot.register(bot.new_cmd("/reply").and_then(|(bot, msg)| {
    let mut text = msg.text.unwrap().clone();
    if text.is_empty() {
        text = "<empty>".into();
    }

    bot.message(msg.chat.id, text).send()
}));

bot.run(&mut lp).unwrap();
```

```rust
// tbot
use tbot::{prelude::*, Bot};

let mut bot = Bot::from_env("BOT_TOKEN");

bot.on_text(|context| {
    let reply = context
        .send_message(&context.message)
        .into_future()
        .map_err(|error| eprintln!("Couldn't send a message: {:#?}", error));

    tbot::spawn(reply);
});

bot.polling().start();
```

See how `tbot`'s example is minimal: we only need to import `tbot`, we don't
`unwrap` anything, and update handling is done much simpler. Also notice that
`tbot` enforces error handling, while it's not obvious how to do it in the other
crates.

# Deprecated `tokio_core`

Hope it's a temporary issue, but the other crates seem to require the deprecated
`tokio_core` crate. `tbot` uses only `tokio` under the hood. Even more, we're
going to move to `async/await` as soon as it stabilizes.

<!--
  TODO: As soon as webhook support comes out, we should mention it here.
  Same for multipart support and that `telegram-bot` doesn't support it.
-->

# `telebot`: strings everywhere

At the time of writing this, `telebot` was the most popular crate for Bots API
recently. For us, [this small commit][telebot-commit] is enough not to use
`telebot`:

```diff
Telegram does not support "Text" as a parse_mode

@@ -27,1 +27,1 @@ fn main() {
- bot.message(msg.chat.id, text).parse_mode("Text").send()
+ bot.message(msg.chat.id, text).send()
```

Really, use strings when enums should be used? This leads to runtime errors,
while they could be prevented at compile time. `tbot` only accepts
`types::ParseMode`, and you won't get runtime errors because you used a string.
If you get a runtime type error when working with `tbot`, then it's a [bug].

[telebot-commit]: https://github.com/bytesnake/telebot/commit/6f33c627644aa477ea
[bug]: https://gitlab.com/snejugal/tbot/issues

We'll look into ways to set up webhooks for your bot built with `tbot`, for both
testing on your local machine and running on your server. Note that the first
steps are general and applicable not only to `tbot`.

# Preparing your local machine

You can use [`ngrok`] to test webhooks on your local machine. Follow their steps
to install it on your computer. After you installed it, run this command:

```bash
./ngrok http $PORT
```

Remember the port you're listening on and the HTTPS server `ngrok` provided you
with. Then you can go to the [next step].

[`ngrok`]: https://ngrok.com

# Preparing your server

First of all, you need to know your server's address so Telegram will be able to
send you updates. Second, you need to get an HTTPS certificate. A trusted
certificate requires one step less to set up webhooks, but it costs money, so
you can [generate][self signed] a self-signed certificate and only add one line
of code.

After this, you'll need to set up a reverse proxy between Telegram and `tbot`.
This is needed because Telegram sends updates only over HTTPS, while `tbot`
does not support HTTPS ([why?]). The proxy must accept Telegram's requests,
redirect it to `tbot`'s server over HTTP, accept `tbot`'s response, and send it
back to Telegram over HTTPS. You can do this with `nginx`, but you may use
whatever you want. Do not forget the port the proxy will redirect requests to,
you'll need it to set up `tbot`. Remember that the proxy must listen on one of
these ports: `443`, `80`, `88`, `8443`, as per [docs][setWebhook].

After you set up a proxy, you can proceed to the [next step].

[self signed]: https://core.telegram.org/bots/self-signed
[setWebhook]: https://core.telegram.org/bots/api#setwebhook
[next step]: #preparing-tbot
[why?]: #why-wont-tbot-support-https

# Preparing `tbot`

Now it's time to tell `tbot` to use webhooks. Most probably, you already have
something like this:

```rust
use tbot::*;

fn main() {
  let mut bot = Bot::new();
  // ...
  bot.polling().start();
}
```

By now you should know the address where updates will be sent and the port
to which Telegram's requests will be redirected. Given that they're in
`URL: &str` and `PORT: u16` constants, replace the `bot.polling().start()` line
so your code looks like this:

```rust
use tbot::*;

fn main() {
  let mut bot = Bot::new();
  // ...
  bot.webhook(URL, PORT).start();
}
```

If you have a self-signed certificate, then you'll have to send its _public_
key (not the private key nor the certificate itself, that's what your proxy
deals with) to Telegram. Just add a [`certificate`][`Webhook::certificate`]
call:

```rust
bot
  .webhook(URL, PORT)
  .certificate(include_str!("path/to/public/key.pem"))
  .start();
```

Now you can start your bot, and it will use webhooks. Hooray!

[`Webhook::certificate`]: https://docs.rs/tbot/0.1.0/struct.Webhook#method.certificate

# Notes

- If you have set a polling error handler, then you won't need it with
  webhooks. These are  _polling_ errors, after all.
- If `tbot` encounters an error while setting webhooks, it will simply panic,
  as we consider those unrecoverable errors. There will be an error message
  explaining what exactly went wrong.
- `tbot` will listen on `127.0.0.1` by default, you can change it with
  [`Webhook::ip`].
- `tbot` always listens only to POST requests to `/`.

[`Webhook::ip`]: https://docs.rs/tbot/0.1.0/struct.Webhook#method.ip

# Why won't `tbot` support HTTPS?

We're not against supporting HTTPS, we believe it would only make things easier
for bot developers. The reason is simple: `hyper` does not support it out of the
box, `actix-web` seems overhead to us (change our minds), and implementing HTTPS
support on our own is definetely out of `tbot`'s scope. If you know how to
support HTTPS (preferably without pulling other crates than `tbot` already
pulls), feel free to open a merge request on [our GitLab].

[our Gitlab]: https://gitlab.com/snejugal/tbot

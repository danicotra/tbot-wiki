We'll look into different ways to send a message depending on your needs.

# Replying in the same chat

If you received a message in a chat and you want to send a response, use
`context.send_message`.

```rust
use tbot::{prelude::*, Bot};

let mut bot = Bot::from_env("BOT_TOKEN");

bot.on_text(|context| {
    let reply = context
        .send_message("Got your message")
        .into_future()
        .map_err(|error| eprintln!("Couldn't send a message: {:#}", error));

    tbot::spawn(reply);
});

bot.polling().start();
```

If you'd like to send in reply to the received message, you can use
`send_message_in_reply`. There are also respective methods for sending
attachments, like `send_photo`.

# Sending a message in another chat

To send a message in another chat, you'll need to use [`Bot::send_message`]:

```rust
use tbot::{prelude::*, Bot};

// Use the ID you need
const CHAT: i64 = 0;

let mut bot = Bot::from_env("BOT_TOKEN");

bot.on_text(|context| {
    let reply = context.bot
        .send_message(CHAT, "Got someone's message")
        .into_future()
        .map_err(|error| eprintln!("Couldn't send a message: {:#}", error));

    tbot::spawn(reply);
});

bot.polling().start();
```

Note that we used `context.bot` as `bot` was consumed by `bot.polling()`.

If you're sending a message to a channel, you may use a `&str` instead of an
`i64`:

```rust
const CHAT: &str = "@username";
```

`tbot` can deal with both these types.

# For a notifying bot

If you're making a bot that simply sends notifications, you may not need
constructing a [`Bot`], in which case, you can construct the method directly.

```rust
use tbot::{methods::SendMessage, prelude::*};

const CHAT: i64 = 0;

let request = SendMessage::new(
    env!("BOT_TOKEN"),
    CHAT,
    "Hello world, hope you're listening...",
)
.into_future()
.map_err(|error| eprintln!("Couldn't send a message: {:#?}", error));

tbot::run(request);
```

[`Bot::send_message`]: https://docs.rs/tbot/0.1.0/struct.Bot#method.send_message
[`Bot`]: https://docs.rs/tbot/0.1.0/struct.Bot#method.send_message
